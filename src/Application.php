<?php
declare(strict_types=1);

namespace SilenZ\Web;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

use GuzzleHttp\Psr7\ServerRequest;
use GuzzleHttp\Psr7\Response;

use function GuzzleHttp\Psr7\stream_for;
use function Http\Response\send;

class Application extends Router {

  private $container;

  public function __construct($container) {
    $this->container = $container;
  }

  public function run(ServerRequestInterface $request = null) {
    $request = $request ?? ServerRequest::fromGlobals();
    $response = null;
    try {
      $response = $this($request);
    }
    catch (\Exception $exception) {
      $errorHandler = $this->container->has(ErrorHandler::class) 
        ? $this->container->get(ErrorHandler::class) 
        : new DefaultErrorHandler();
      $response = $errorHandler->handle($exception, $request);
    }
    finally {
      send($response);
    }
  }

  public function __invoke(ServerRequestInterface $request, $response = null, $next = null) {
    return parent::__invoke($request->withAttribute("container", $this->container), null, $next);
  }

  private static function getErrorResponse($exception) {
     
  }
}

class DefaultErrorHandler {
  public function handle(\Exception $exception, ServerRequestInterface $request) {
    $output = "Internal server error";
    if (strtolower(getenv("WEB_ENV")) !== "production") {
      $errorMessage = $exception->getMessage();
      $stackTrace = $exception->getTraceAsString();
      $output = $output . " '{$errorMessage}':\n${stackTrace}";
    }
    $response = new Response();
    return $response
      ->withStatus(500)
      ->withHeader("Content-Type", "text/plain")
      ->withBody(stream_for($output));
  }
}