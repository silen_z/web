<?php
declare(strict_types=1);

namespace SilenZ\Web;

final class Handler {
  private function __construct(){}

  public static function method($object, $method) {
    return function ($request, $response, $next) use($object, $method) {
      return call_user_func([$object, $method], $request, $response, $next);
    };
  }
}