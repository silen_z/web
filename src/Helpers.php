<?php
declare(strict_types=1);

namespace SilenZ\Web;

use GuzzleHttp\Psr7;
use function GuzzleHttp\Psr7\stream_for;

class Helpers extends  {
  public static function redirect(Psr7\Response $response, string $url) {
    return $response->withHeader("Location", $url);
  }

  public static function withJson(Psr7\Response $response, $serializable) {
    return $response
      ->withHeader("Content-Type", "application/json")
      ->withBody(stream_for(json_encode($serializable)));
  }
}
