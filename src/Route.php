<?php
declare(strict_types=1);

namespace SilenZ\Web;

use SilenZ\Web\Path\IPath;
use SilenZ\Web\Path\MatchedPath;

/** 
 * Stores HTTP method, path to match and handler,
 * returns handler with matched path and uriParams
 */
class Route {

  /** @var IPath $path Represents pattern to match against requests */
  private $path;

  /** @var callable $hanlder function or invokable class
   *  that should be included in middleware stack in case $path matches
   */
  public $handler;

  /** @var string|null $method HTTP method to match,
   *  middleware route matches all methods and $method = null
   */
  private $method;

  /** @var boolean $isMiddleware indicates whether to match route as middleware */
  private $isMiddleware;

  /** @var MatchedPath|null $match populated with matchedPath and uriParams after matching */
  public $match = null;

  /**
   * @param IPath $path path
   * @param string|callable $handler handler
   * @param string $method HTTP method
   */
  public function __construct(IPath $path, $handler, $method = null) {
    $this->handler = $handler;
    $this->method = $method;
    $this->path = $path;
    $this->isMiddleware = $method === null;
  }

  /**
   * @param string $path request path to match against
   * @param string $method request method to match against
   * @return boolean if route matcher server request
   */
  public function match($path, $method) {
    if (
      !$this->isMiddleware
      && $this->method !== "*"
      && $this->method !== $method
    ) {
      return false;
    }

    $this->match = $this->path->match($path, $this->isMiddleware);
    return $this->match !== null;
  }
}
