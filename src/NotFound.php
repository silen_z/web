<?php
declare(strict_types=1);

namespace SilenZ\Web;

use function GuzzleHttp\Psr7\stream_for;

class NotFound {

  public function __invoke($request, $response) {
    $method = strtoupper($request->getMethod());
    $path = $request->getUri()->getPath();
    return $response
      ->withStatus(404)
      ->withHeader("Content-Type", "text/plain")
      ->withBody(stream_for("can’t {$method}: {$path}"));
  }
}