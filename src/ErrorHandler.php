<?php
declare(strict_types=1);

namespace SilenZ\Web;

use Psr\Http\Message\ServerRequestInterface;

interface ErrorHandler {
  public function handle(\Exception $exception, ServerRequestInterface $request);
}