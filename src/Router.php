<?php
declare(strict_types=1);

namespace SilenZ\Web;

use SilenZ\Web\Path\Path;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Container\ContainerInterface;
use Relay\RelayBuilder;
use GuzzleHttp\Psr7\Response;

use function GuzzleHttp\Psr7\stream_for;

class Router {

  private $routes = [];

  public function __invoke(ServerRequestInterface $request, $response = null, $next = null) {
    // strip already matched path
    $path = "/" . trim($request->getUri()->getPath(), "/");
    $matched = $request->getAttribute(ResolvedHandler::MATCHED_PATH);
    if ($matched) {
      $path = self::stripMatchedPath($path, $matched);
    }
  
    $container = $request->getAttribute("container");

    // handle not found routes
    $routes = $this->routes;
    $routes[] = new Route(Path::always(), $next ?? new NotFound());

    // Find matching routes
    $matchingRoutes = array_filter($routes, function($route) use ($request, $path) {
      return $route->match($path, $request->getMethod());
    });

    // convert route to handler-middleware
    $builder = new RelayBuilder(function ($route) use ($container) {
      return new ResolvedHandler(self::fromContainer($route->handler, $container), $route->match);
    });

    $middlewareStack = $builder->newInstance($matchingRoutes);
    return $middlewareStack($request, $response ?? new Response());
  }

  public function add(string $method, $path, $handler, string $name = null) {
    $route = new Route(Path::from($path), $handler, $method);
    $this->routes[] = $route;
    return $this;
  }

  public function attach($pathOrMiddleware, $middleware = null) {
    if ($middleware === null) {
      $this->attachMiddleware(Path::always(), $pathOrMiddleware);
    } else {
      $this->attachMiddleware(Path::from($pathOrMiddleware), $middleware);
    }
    return $this;
  }

  private function attachMiddleware($path, $middleware) {
    if (!is_array($middleware)) $middleware = [$middleware];
    foreach ($middleware as $m) {
      $this->routes[] = new Route($path, $m);
    }
  }

  /**
   * resolves handler from container if needed
   * @param ContainerInterface $container app container
   * 
   */
  private static function fromContainer($handler, ContainerInterface $container = null) {
    if (is_callable($handler)) {
      return $handler;
    }
    if ($container !== null && $container->has($handler)) {
      return $container->get($handler);
    }
    throw new \Exception("Handler must callable or resolvable from container, '".gettype($handler)."' given");
  }

  private static function stripMatchedPath($path, $matched) {
    $s = substr($path, strlen($matched));
    return "/" . trim($s, "/");
  }

  /**
   * Helpers for common HTTP methods
   */

  public function get($path, $handler, $name = null) {
    return $this->add("GET", $path, $handler, $name);
  }

  public function post($path, $handler, $name = null) {
    return $this->add("POST", $path, $handler, $name);
  }

  public function put($path, $handler, $name = null) {
    return $this->add("PUT", $path, $handler, $name);
  }

  public function patch($path, $handler, $name = null) {
    return $this->add("PATCH", $path, $handler, $name);
  }

  public function delete($path, $handler, $name = null) {
    return $this->add("DELETE", $path, $handler, $name);
  }

  public function all($path, $handler, $name = null) {
    return $this->add("*", $path, $handler, $name);
  }
}

class ResolvedHandler {

  const MATCHED_PATH = "matchedPath";
  const URI_PARAMS = "uriParams";

  private $handler;
  private $match;
  
  public function __construct($handler, $match) {
    $this->handler = $handler;
    $this->match = $match;
  }
  
  public function __invoke($request, $response, $next) {
    $request = $request
      ->withAttribute(self::MATCHED_PATH, $this->match->matchedPath)
      ->withAttribute(self::URI_PARAMS, $this->match->uriParams);
    return call_user_func($this->handler, $request, $response, $next);
  }
}