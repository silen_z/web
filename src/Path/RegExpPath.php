<?php
declare(strict_types=1);

namespace SilenZ\Web\Path;

class RegExpPath extends Path {
  public function match(string $path, $asMiddleware) {
    $matches = preg_match($this->path, $path);
    if (!$matches) return null;
    return new MatchedPath($matches[0]);
  }
}