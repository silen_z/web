<?php
declare(strict_types=1);

namespace SilenZ\Web\Path;

class MatchedPath {
  public $matchedPath;
  public $uriParams;

  public function __construct($matchedPath = "", $uriParams = []) {
    $this->matchedPath = $matchedPath;
    $this->uriParams = $uriParams;
  }
}