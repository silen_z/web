<?php
declare(strict_types=1);

namespace SilenZ\Web\Path;

interface IPath {

  /**
   * @param string $path request path to match
   * @param boolean $asMiddleware whether to match as middleware (non-strict end)
   * @return MatchedPath|null if the path matches return match informations such as matchedPath and uriParams
   */
  public function match(string $path, $asMiddleware);
}