<?php
declare(strict_types=1);

namespace SilenZ\Web\Path;

abstract class Path implements IPath {

  protected $path;

  public function __construct($path) {
    $this->path= $path;
  }

  public static function from($path) {
    if ($path instanceof IPath) return $path;
    if (is_callable($path)) return new CallbackPath($path);
    if (!is_string($path)) throw new InvalidPathException($path);
    if (substr($path, 0, 3) === "re:") {
      return new RegExpPath(substr($path, 3));
    }
    return new PatternPath($path);
  }

  public static function always() {
    return new AlwaysMatchPath();
  }
}