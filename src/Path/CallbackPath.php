<?php
declare(strict_types=1);

namespace SilenZ\Web\Path;

class CallbackPath extends Path {
  public function match(string $path, $asMiddleware) {
    return call_user_func($this->path, [$path, $asMiddleware]);
  }
}
