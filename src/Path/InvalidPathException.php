<?php
declare(strict_types=1);

namespace SilenZ\Web\Path;

class InvalidPathException extends \Exception {
  public function __construct($path) {
    parent::__construct("Can't resolve path expression of type: ". gettype($path). ", use ");
  }
}