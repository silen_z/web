<?php
declare(strict_types=1);

namespace SilenZ\Web\Path;

class AlwaysMatchPath implements IPath {
  public function match(string $path, $asMiddleware) {
    return new MatchedPath("/");
  }
}
