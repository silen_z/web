<?php
declare(strict_types=1);

namespace SilenZ\Web\Path;

class PatternPath extends Path {
  public function match(string $path, $asMiddleware) {

    //echo "matching: route: {$this->path} request: {$path} \n";

    $params = [];
    $re = PathToRegexp::convert($this->path, $params, [
      'sensitive' => false,
      'strict' => false,
      'end' => !$asMiddleware
    ]);
  
    $matches = PathToRegexp::match($re, $path);
    if ($matches === null) return null;
    $uriParams = [];
    foreach ($params as $i => $param) {
      $uriParams[$param['name']] = $matches[1 + $i];
    }
    return new MatchedPath($matches[0], $uriParams);
  }
}