<?php

use PHPUnit\Framework\TestCase;
use SilenZ\Web\{Application, Router, ErrorHandler};
use GuzzleHttp\Psr7\{Response, ServerRequest};
use Psr\Container\ContainerInterface;

final class ApplicationTest extends TestCase {

  public function testIsRouter() {
    $this->assertInstanceOf(Router::class, new Application());
  }

  /**
  * @runInSeparateProcess
  */   
  public function testResolvesHanldersAndPassesContainerToResponse(): void
  {   
    $handler = function ($req, $res) {
      $this->assertInstanceOf(ContainerInterface::class, $req->getAttribute("container"));
      return $res;
    };

    $app = new Application([
      'handler' => function () use ($handler) {
        return $handler;
      }
    ]);

    $app->attach("handler");
    $app->run(new ServerRequest("GET", "http://example.com"));
  }

  /**
  * @runInSeparateProcess
  */
  public function testResolvesAndInvokesErrorHandler() {
    
    $errorHandler = $this->createMock(ErrorHandler::class);
    $request = new ServerRequest("GET", "http://example.com");
    $exception = new \Exception();

    $app = new Application([
      ErrorHandler::class => function () use ($errorHandler) {
        return $errorHandler;
      }
    ]);

    $app->attach(function ($req, $res) use ($exception) {
      throw $exception;
    });

    $errorHandler->expects($this->once())
    ->method('handle')
    ->with($this->identicalTo($exception), $this->identicalTo($request))
    ->will($this->returnValue(new Response()));

    $app->run($request);
  }
}