<?php

use PHPUnit\Framework\TestCase;
use SilenZ\Web\Router;
use GuzzleHttp\Psr7\{Response, ServerRequest};
use DI\ContainerBuilder;

final class RouterTest extends TestCase {

  public function testMatchesRoutes(): void
  {
    $router = new Router();

    $getResponse = new Response();
    $router->get("/", function() use ($getResponse){
      return $getResponse;
    });

    $postResponse = new Response();
    $router->post("/submit", function() use ($postResponse){
      return $postResponse;
    });

    $anyResponse = new Response();
    $router->add("*", "/any", function() use ($anyResponse){
      return $anyResponse;
    });

    $customResponse = new Response();
    $router->add("CUSTOM_METHOD", "/custom", function() use ($customResponse){
      return $customResponse;
    });

    $this->assertSame(
      $getResponse,
      $router(new ServerRequest("GET", "http://example.com"))
    );

    // ignores trailing slash
    $this->assertSame(
      $getResponse,
      $router(new ServerRequest("GET", "http://example.com/"))
    );

    // doesn't match wrong methods
    $this->assertEquals(
      404,
      $router(new ServerRequest("GET", "http://example.com/submit"))->getStatusCode()
    );

    $this->assertSame(
      $anyResponse,
      $router(new ServerRequest("GET", "http://example.com/any"))
    );

    $this->assertSame(
      $anyResponse,
      $router(new ServerRequest("PATCH", "http://example.com/any"))
    );

    // handles custom methods
    $this->assertSame(
      $customResponse,
      $router(new ServerRequest("CUSTOM_METHOD", "http://example.com/custom"))
    );
    $this->assertEquals(
      404,
      $router(new ServerRequest("GET", "http://example.com/custom"))->getStatusCode()
    );
    $this->assertSame(
      $anyResponse,
      $router(new ServerRequest("CUSTOM_METHOD", "http://example.com/any"))
    );
  }

  public function testIgnoresSlash(): void {
    $router = new Router();

    $getResponse = new Response();
    $router->get("/", function() use ($getResponse){
      return $getResponse;
    });

    $this->assertSame(
      $getResponse,
      $router(new ServerRequest("GET", "http://example.com"))
    );

    $this->assertSame(
      $getResponse,
      $router(new ServerRequest("GET", "http://example.com/"))
    );
  }

  public function testHandlesNesting(): void
  {
    $router = new Router();
    $expectedResponse = new Response();

    // request should just fall to next nested router
    $nestedFallthrough = new Router();
    $router->attach("/nested", $nestedFallthrough);

    // request should match on /nested/test
    $nestedRouter = new Router();
    $nestedRouter->get("/test", function () use ($expectedResponse) {
      return $expectedResponse;
    });
    $router->attach("/nested", $nestedRouter);

    $this->assertEquals(
      $expectedResponse,
      $router(new ServerRequest("GET", "http://example.com/nested/test"))
    );

    // ignores trailing slash
    $this->assertEquals(
      $expectedResponse,
      $router(new ServerRequest("GET", "http://example.com/nested/test/"))
    );

    // corectly handles not found routes
    $this->assertEquals(
      404,
      $router(new ServerRequest("GET", "http://example.com/nested/unknown"))->getStatusCode()
    );

  }

  public function testHandlesNotFound(): void
  {
    $router = new Router();
    $router->get("/test", function($req, $res) {
      return $res;
    });
    $routerResponse = $router(new ServerRequest("GET", "http://example.com"));
    $this->assertEquals(404, $routerResponse->getStatusCode());
  }

  public function testMatchesInOrder(): void
  {
    $router = new Router();
    $expectedResponse = new Response();
    $router->attach(function () use ($expectedResponse) {
      return $expectedResponse;
    });
    $router->attach(function () {
      return new Response();
    });
    $this->assertSame(
      $expectedResponse,
      $router(new ServerRequest("GET", "http://example.com") )
    );
  }
  
  public function testLetsMiddlewarePass(): void
  {
    $router = new Router();
    $expectedResponse = new Response();
    $router->attach(function ($request, $response, $next) {
      return $next($request, $response);
    });
    $router->attach(function ($request, $response, $next) use ($expectedResponse) {
      return $expectedResponse;
    });
    $this->assertSame(
      $expectedResponse,
      $router(new ServerRequest("GET", "http://example.com") )
    );
  }

  public function testCanBeChained() {
    $router = new Router();
    $expectedResponse = new Response();
    $handler = function() use ($expectedResponse){
      return $expectedResponse;
    };

    $router
      ->attach("/route1", $handler)
      ->get("/route2", $handler);

    $this->assertSame(
      $expectedResponse,
      $router(new ServerRequest("GET", "http://example.com/route1"))
    );

    $this->assertSame(
      $expectedResponse,
      $router(new ServerRequest("GET", "http://example.com/route2"))
    );

  }
}